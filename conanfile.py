import os
from conans import ConanFile, VisualStudioBuildEnvironment, tools
from conans.tools import download, unzip

class PremakeConan(ConanFile):
    name = "premake"
    version = "5.0.0-alpha11"
    license = "MIT"
    url = "https://bitbucket.org/genvidtech/conan-premake"
    settings = "os"
    branch = "stable"
    description = "Premake Installer"

    def build(self):
        self.output.warn("BUILD...")
        self.output.warn(os.getcwd())

        archive = "premake.zip"
        baseurl = "https://github.com/premake/premake-core/releases/download/v{version}".format(version=self.version)
        if self.settings.os == "Windows":
            url = baseurl + "/premake-{version}-windows.zip".format(version=self.version)
            download(url, archive)
            unzip(archive)
            return

        url = baseurl + "/premake-{version}-src.zip".format(version=self.version)
        download(url, archive)
        unzip(archive)
        os.unlink(archive)
        
        if self.settings.os == "Macos":
            
            self.run("make", cwd="premake-{version}/build/gmake.macosx".format(version=self.version))

        #elif self.settings.os == "Linux":
        #    os.chdir("premake-5.0.0-alpha9/build/gmake.unix")
        #    self.run("make")h
        #    os.chdir("../..")

    def package(self):
        self.output.warn("PACKAGE...")
        self.output.warn(os.getcwd())
        if self.settings.os == "Windows":
            self.copy("premake5.exe", dst="bin")
        else:
            self.copy("*", dst="bin", src="premake-{version}/bin/release".format(version=self.version))

    def package_info(self):
        self.env_info.path.append(os.path.join(self.package_folder, "bin"))
        self.cpp_info.includedirs = []
        self.cpp_info.libdirs = []
        self.cpp_info.bindirs = []
