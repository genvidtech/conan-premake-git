from conans import ConanFile, RunEnvironment, tools
import os

channel = os.getenv("CONAN_CHANNEL", "1.4.0")
username = os.getenv("CONAN_USERNAME", "genvidtech")


class PremakeTestConan(ConanFile):
    settings = "os"
    requires = (
        "premake/5.0.0-alpha11@%s/%s" % (username, channel), 
    )
    generators = "txt"

    def build(self):
        pass

    def test(self):
      env_build = RunEnvironment(self)
      with tools.environment_append(env_build.vars):
          self.run("premake5 --version")
